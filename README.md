# Formação Microsoft Power BI Profissional 2022



## Proposta

Este repositório foi criado com o intuito de submeter os arquivos desenvolvidos durante a realização do Curso "Formação Microsoft Power BI Profissional" realizado na Plataforma Udemy durante o mês de dezembro de 2022.

O projeto foi desenvolvido em simultaneidade com o andamento do curso na ferramenta Power BI Desktop. Versão da ferramenta: 2.112.603.0 64-bit (dezembro de 2022).



Caso se interesse o Link do Curso encontra-se em: https://www.udemy.com/course/formacao-power-bi-pro/
